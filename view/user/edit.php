<?php
if (isset($_GET['id'])){
    include_once ('../include/header.php');
    include_once ('../../src/Location.php');

    session_start();

    $object = new Location();

    try{
        $id = $_GET['id'];
        $_SESSION['id'] = $id;
//        echo $id;
        $data = $object->getUser($id);
    }
    catch(PDOException $ex){
        echo 'error';
    }

//var_dump($data);

    if (sizeof($data) > 0){
        echo '<div class="container">';
        echo '<div class="col-md-8 m-auto">';
            echo '<h1>======== Edit User Data ========</h1>';
            echo '<hr>';
            echo '<form id="assessmentForm" action="" method="POST">';
                echo '<div class="form-group">';
                    echo '<label for="title">Location Title</label>';
                    echo '<input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" value="'.htmlentities(htmlspecialchars($data[0]['title'])).'" required>';
                echo '</div>';
                echo '<div class="form-group">';
                    echo '<label for="details">Descriptions</label>';
                    echo '<textarea type="text" class="form-control" id="details" name="details" rows="5" required>'.htmlentities(htmlspecialchars($data[0]['details'])).'</textarea>';
                echo '</div>';
                echo '<button style="margin: 5px" type="reset" class="btn btn-primary">Reset</button>';
            echo '</form>';
        echo '<button style="margin: 5px" type="submit" class="btn btn-success" id="submit" name="submit" value="submit">Update</button>';
        echo '<a style="margin: 5px" href="" class="btn btn-primary">Cancel</a>';

            echo '<div id="fetch"></div>';
        echo '</div>';
        echo '</div>';
    }
}
?>

    <script type="text/javascript">
        $(document).ready(function (){
            $('#submit').click(function (){
                $.ajax({
                    url: 'view/admin/update.php',
                    type: 'POST',
                    data: $('#assessmentForm').serialize(),

                    success: function (response){
                        if(response == 'success') {
                            alert('Data updated successful!!');
                            window.location.href = "";
                        }else {
                            alert("Couldn't update the requested data!!");
                            // console.log(response);
                            window.location.href = "";
                        }
                    }
                })
                $('#assessmentForm :input').val('');
            });
        });
    </script>

<?php include_once ('../include/footer.php');?>