<?php
if (empty($_POST)){
    header('location: ../../');
}
else if ($_POST['title'] != "" && $_POST['details'] != ""){
    include_once ('../../src/Location.php');
    session_start();

//    var_dump($_POST);

    $object = new Location();

    try{
        $object->set($_POST);
        $object->update($_SESSION['id']);
        session_destroy();
        echo 'success';
    }
    catch(PDOException $ex){
        echo 'error';
    }
}