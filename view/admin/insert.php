<?php
if (empty($_POST)){
    header('location: ../../');
}
else if ($_POST['title'] != "" && $_POST['details'] != ""){
    include_once ('../../src/Location.php');

//    var_dump($_POST);

    $object = new Location();

    try{
        $object->set($_POST);
        $object->store();
        echo 'success';
    }
    catch(PDOException $ex){
        echo 'error';
    }
}