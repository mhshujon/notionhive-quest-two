<?php
if (empty($_POST)){
    echo 'error';
}
else if (isset($_POST['id'])){
    include_once ('../../src/Location.php');

//    var_dump($_POST);

    $object = new Location();

    try{
        $object->delete($_POST['id']);
        echo 'success';
    }
    catch(PDOException $ex){
        echo 'error';
    }
}