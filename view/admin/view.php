<?php
include_once ('../include/header.php');
include_once ('../../src/Location.php');

//    var_dump($_POST);

$object = new Location();

try{
    $data = $object->index();
}
catch(PDOException $ex){
    echo 'error';
}

//var_dump($data);

if (sizeof($data) > 0){
    echo '<table class="table" style="margin-top: 50px">';
    echo '<thead>';
    echo '<tr>';
    echo '<th scope="col">#</th>';
    echo '<th scope="col">Title</th>';
    echo '<th scope="col">Details</th>';
    echo '<th scope="col">Update Action</th>';
    echo '<th scope="col">Delete Action</th>';
    echo '</tr>';
    echo '</thead>';
    echo '<tbody>';
    for ($i=0; $i<sizeof($data); $i++){
        echo '<tr>';
        echo '<th scope="row">'.($i+1).'</th>';
        echo '<td>'.htmlentities(htmlspecialchars($data[$i]['title'])).'</td>';
        echo '<td>'.htmlentities(htmlspecialchars($data[$i]['details'])).'</td>';
        echo '<td><a class="btn btn-warning" href="view/user/edit.php?id='.htmlentities(htmlspecialchars($data[$i]['id'])).'">Edit</a></td>';
        echo '<td><button onclick="deleteData('.htmlentities(htmlspecialchars($data[$i]['id'])).')" class="btn btn-danger">Delete</button></td>';
        echo '</tr>';
    }
    echo '</tbody>';
    echo '</table>';
}
include_once ('../include/footer.php');