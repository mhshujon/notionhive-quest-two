<?php include_once ('view/include/header.php');?>
    <div class="container">
        <div class="col-md-8 m-auto">
            <h1>======== Assessment Two ========</h1>
            <hr>
            <form id="assessmentForm" action="" method="POST">
                <div class="form-group">
                    <label for="title">Location Title</label>
                    <input type="text" class="form-control" id="title" name="title" aria-describedby="emailHelp" placeholder="Enter location title" required>
                </div>
                <div class="form-group">
                    <label for="details">Descriptions</label>
                    <textarea type="text" class="form-control" id="details" name="details" placeholder="Description" rows="5" required></textarea>
                </div>
            </form>
            <button type="submit" class="btn btn-primary" id="submit" name="submit" value="submit">Submit</button>

            <div id="fetch"></div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function (){
            $('#fetch').load('view/admin/view.php').fadeIn('slow');
            $('#submit').click(function (){
                $.ajax({
                    url: 'view/admin/insert.php',
                    type: 'POST',
                    data: $('#assessmentForm').serialize(),

                    success: function (response){
                        if(response == 'success') {
                            $('#fetch').load('view/admin/view.php').fadeIn('slow');
                            alert('Data insertion successful!!');

                        }else {
                            alert('Data insertion failed!!');
                        }
                    }
                })
                $('#assessmentForm :input').val('');
            });
        });

        // setInterval(function (){
        //     $('#fetch').load('view/admin/view.php').fadeIn('slow');
        // }, 1000);

        function deleteData(id){
            const confirmation = confirm('Are you sure?')
            if (confirmation){
                $.ajax({
                    type: 'POST',
                    url: 'view/admin/delete.php',
                    data: {id: id},

                    success: function (response){
                        $('#fetch').load('view/admin/view.php').fadeIn('slow');
                        alert('Data deletion successful!!');
                    }
                });
            }
        }
    </script>
<?php include_once ('view/include/footer.php');?>