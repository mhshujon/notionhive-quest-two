<?php

class Connection
{
    private $user = 'root';
    private $pass = '';
    protected $con;

    public function __construct()
    {
        try {
            $this->con = new PDO('mysql:host=127.0.0.1;dbname=notionhive_db', $this->user, $this->pass);

        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}