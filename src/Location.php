<?php
include_once ('Connection.php');

class Location extends Connection
{
    private $title;
    private $details;

    public function set($data = array()){
        if (array_key_exists('title', $data)) {
            $this->title = trim(filter_var($data['title'], FILTER_SANITIZE_STRING));
        }
        if (array_key_exists('details', $data)) {
            $this->details = trim(filter_var($data['details'], FILTER_SANITIZE_STRING));
        }
////        var_dump($this);
//        return $this->title;
    }

    public function store(){
        try{
            $stmt = $this->con->prepare("INSERT INTO `location` (`title`, `details`) VALUES (:title, :details)");
            $result =  $stmt->execute(array(
                ':title' => $this->title,
                ':details' => $this->details
            ));

//            var_dump('this->title');
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
//                header('postDetails:index.php');
//                echo $this->title;
//            }
        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function index(){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `location`");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function update($id){
        try{
            $stmt = $this->con->prepare("UPDATE `notionhive_db`.`location` SET `title` = :title, `details` = :details WHERE `id` = '$id';");

            $result =  $stmt->execute(array(
                ':title' => $this->title,
                ':details' => $this->details
            ));
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr = $stmt->errorInfo();
//            print_r($arr);
//            if($result){
////                echo 'Yes'.$this->email;
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function delete($id){
        try{
            $stmt = $this->con->prepare("DELETE FROM `location` WHERE id=:id");
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
            $stmt->execute();
//            echo "\nPDOStatement::errorInfo():\n";
//            $arr1 = $stmt->errorInfo();
//            print_r($arr1);
//            if($stmt){
//                $_SESSION['delete'] = 'Data successfully Deleted !!';
//                header('postDetails:index.php');
//            }

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }

    public function getUser($id){
        try{
            $stmt = $this->con->prepare("SELECT * FROM `location` WHERE `id`='$id'");
            $stmt->execute();
            return $stmt->fetchAll(PDO::FETCH_ASSOC);

        }catch (PDOException $e) {
            echo "There is some problem in connection: " . $e->getMessage();
        }
    }
}